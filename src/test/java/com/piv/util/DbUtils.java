package com.piv.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.google.common.collect.Lists;
import com.google.common.io.Files;

/*
 * Utility to create local HSQLDB schema from CSV files
 */
public class DbUtils {

	// need to be able to do this:
	// 1. create schema in local HSQLDB from create script(s)
	// 2. create SQL insert scripts from CSV files (e.g. export from tables in SQLDeveloper)
	// 3. populate local HSQLDB from insert SQL script. (can be combined w/ 1)

	static final String dbDir = "src/test/resources/dbData";

	public static void main(final String[] args) throws Exception {

		final String baseDir = "src/test/resources/db-test-data/";

		createNewSchemaInLocalHSQLDB( //
		        baseDir + "create_tables.ddl", //
		        baseDir + "vehicle_test_data.csv", //
		        baseDir + "route_test_data.csv" //
		);

	}

	public static void createNewSchemaInLocalHSQLDB(String ddlFile, String... tableDataCSVFiles) throws Exception {

		FileUtils.deleteDirectory(new File(dbDir));
		final Connection con = DriverManager.getConnection("jdbc:hsqldb:" + dbDir + "/test", "sa", "");

		final String ddl = Files.toString(new File(ddlFile), // "src/test/resources/db-test-data/create_tables.ddl"),
		        Charset.defaultCharset());
		con.createStatement().execute("SET DATABASE SQL SYNTAX ORA TRUE");
		con.createStatement().execute(ddl);

		for (final String csvFile : tableDataCSVFiles) {

			// create insert SQL statements from the csv file

			final List<String> insertStats = createInsertSqlFromCsv(csvFile);

			for (final String insertStatement : insertStats) {
				System.out.println("" + insertStatement);
				con.createStatement().execute(insertStatement);
			}

		}

		con.commit();
		con.close();
	}

	static char COL_DELIM = ',';

	public static final List<String> createInsertSqlFromCsv(final String csvFilename) throws Exception {

		final List<String> sqlLines = Lists.newArrayList();
		final List<String> lines = Files.readLines(new File(csvFilename), Charset.defaultCharset());

		final String header = lines.get(0);

		final int tableNameEnd = header.indexOf(':');
		if (tableNameEnd < 0) {
			throw new Exception("No table names found in the CSV header");
		}
		final String tableName = header.substring(0, tableNameEnd).trim();
		final String columnNames = header.substring(tableNameEnd + 1).replaceAll("'", "");
		// final String columnNames = lines.get(0).replaceAll("'", "");
		final int colCount = columnNames.split(",").length;
		for (int i = 1; i < lines.size(); i++) {
			final String ln = lines.get(i);

			final int rowColCount = countColumns(ln, COL_DELIM);
			if (rowColCount != colCount) {
				throw new IOException(
				        "row " + i + " contains " + rowColCount + " but " + colCount + " were expected!\nln:" + ln);
			}
			final String insertSql = String.format("insert into %s(%s) values (%s)", tableName, columnNames, ln);
			sqlLines.add(insertSql);
		}
		return sqlLines;
	}

	static int countColumns(final String line, final char delim) {

		int columnCount = 0;
		boolean inQuote = false;

		for (int i = 0; i < line.length(); i++) {
			final char ch = line.charAt(i);
			// System.out.println((inQuote ? "T" : "F") + " : " + ch);
			if (ch == '\'') {
				inQuote = !inQuote;
			} else if (ch == delim) {
				if (!inQuote) {
					columnCount++;
					// System.out.println("---DELIM---");
				}

			}

		}

		return columnCount + 1;
	}
}
