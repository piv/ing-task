package com.piv.dao;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.piv.model.Vehicle;

@Repository
@Transactional
public class VehicleDao {

	@Resource
	private SessionFactory sessionFactory;

	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings("unchecked")
	public List<Vehicle> getAll() {
		return getSession().createCriteria(Vehicle.class).list();
	}

	public Vehicle findById(final int id) {

		return (Vehicle) getSession().createCriteria(Vehicle.class) //
		        .add(Restrictions.eq(Vehicle.field.id.toString(), id)).uniqueResult();
	}

	public Vehicle findRegNo(final String regNo) {

		return (Vehicle) getSession().createCriteria(Vehicle.class) //
		        .add(Restrictions.eq(Vehicle.field.registration.toString(), regNo))//
		        .uniqueResult();
	}
}
