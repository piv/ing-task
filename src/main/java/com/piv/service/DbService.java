package com.piv.service;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.piv.bean.ServiceState;
import com.piv.dao.VehicleDao;
import com.piv.service.monitoring.MonitoredService;

@Service
public class DbService implements MonitoredService {

	@Autowired
	@Resource
	VehicleDao vehicleDao;

	public DbService() {
	}

	@Override
	public String getId() {
		return "db";
	}

	@Override
	public ServiceState getServiceState() {
		try {
			// Some very basic checks
			vehicleDao.getSession();
			vehicleDao.getAll();
		} catch (HibernateException e) {
			return new ServiceState(getId(), ServiceState.Status.NOT_AVAILABLE, "Database is not available");
		}
		return new ServiceState(getId(), ServiceState.Status.FULLY_FUNCTIONAL, "Database is available");
	}
}