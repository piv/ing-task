package com.piv.service;

import javax.ws.rs.WebApplicationException;

import org.springframework.stereotype.Service;

import com.piv.bean.Address;
import com.piv.bean.Location;
import com.piv.bean.ServiceState;
import com.piv.service.monitoring.MonitoredService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@Service
public class AddressService implements MonitoredService {

	private static final String MOCK_SERVICE_URL = "http://vehicletracking.getsandbox.com/address/%s/%s";

	private static final Location PING_LOCATION = new Location("50.850968", "4.352271");

	private Client client;

	public AddressService() {
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(clientConfig);
	}

	@Override
	public String getId() {
		return "addressService";
	}

	@Override
	public ServiceState getServiceState() {
		// Service availability criteria: ping to the service by using a special location
		try {
			getAddress(PING_LOCATION);
		} catch (RuntimeException e) {
			new ServiceState(getId(), ServiceState.Status.NOT_AVAILABLE, "Address service is not working");
		}
		return new ServiceState(getId(), ServiceState.Status.FULLY_FUNCTIONAL, "Address service is working");
	}

	public Address getAddress(Location loc) {

		WebResource webResource = client.resource(String.format(MOCK_SERVICE_URL, loc.getLat(), loc.getLon()));
		webResource.accept("application/json; charset=utf-8").get(ClientResponse.class);
		ClientResponse response = webResource.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new WebApplicationException();
		}
		Address address = response.getEntity(Address.class);
		return address;
	}

}
