package com.piv.service.monitoring;

import java.util.List;

import com.piv.bean.ServiceState;

public interface ServicesMonitoringService {

	List<MonitoredService> getAllServices();

	MonitoredService getServiceById(String id);

	ServiceState getSystemStatus();

}
