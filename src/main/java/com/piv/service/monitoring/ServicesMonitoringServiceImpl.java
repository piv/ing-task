package com.piv.service.monitoring;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Predicate;
import com.google.common.collect.Collections2;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.piv.bean.ServiceState;

@Service
public class ServicesMonitoringServiceImpl implements ServicesMonitoringService {

	private Map<String, MonitoredService> monitoredServicesMap;

	public ServicesMonitoringServiceImpl(List<MonitoredService> monitoredServices) {
		monitoredServicesMap = Maps.uniqueIndex(monitoredServices, new Function<MonitoredService, String>() {
			@Override
			public String apply(MonitoredService ms) {
				return ms.getId();
			}
		});
	}

	public ServicesMonitoringServiceImpl() {
	}

	@Override
	public List<MonitoredService> getAllServices() {
		return Lists.newArrayList(monitoredServicesMap.values());
	}

	@Override
	public MonitoredService getServiceById(String id) {
		return monitoredServicesMap.get(id);
	}

	@Override
	public ServiceState getSystemStatus() {
		// The simple Whole System Health check is based on the state of its dependent services (MonitoredServices)

		// Query all services state sequentially (not very efficient - could be done async)
		List<ServiceState> servicesState = Lists.transform(getAllServices(),
		        new Function<MonitoredService, ServiceState>() {
			        @Override
			        public ServiceState apply(MonitoredService serv) {
				        return serv.getServiceState();
			        }
		        });

		Collection<ServiceState> failedServices = Collections2.filter(servicesState, new Predicate<ServiceState>() {
			@Override
			public boolean apply(ServiceState state) {
				return state.getStatus().equals(ServiceState.Status.NOT_AVAILABLE);
			}
		});

		if (failedServices.isEmpty()) {
			return new ServiceState("MainApplication", ServiceState.Status.FULLY_FUNCTIONAL,
			        "The Application is working ");
		} else {
			String failureReport = Joiner.on(", ")
			        .join(Collections2.transform(failedServices, new Function<ServiceState, String>() {
				        @Override
				        public String apply(ServiceState serv) {
					        return serv.getServiceId() + ":" + serv.getInfo();
				        }
			        }));

			return new ServiceState("MainApplication", ServiceState.Status.NOT_AVAILABLE, failureReport);
		}

	}

}
