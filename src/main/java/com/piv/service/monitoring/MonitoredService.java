package com.piv.service.monitoring;

import com.piv.bean.ServiceState;

/**
 * All external services which state need to be monitored have to implement this interface
 */
public interface MonitoredService {

	String getId();

	ServiceState getServiceState();
}
