package com.piv.service;

import javax.ws.rs.WebApplicationException;

import org.springframework.stereotype.Service;

import com.piv.bean.Location;
import com.piv.bean.ServiceState;
import com.piv.service.monitoring.MonitoredService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;

@Service
public class TrackingService implements MonitoredService {

	private static final String MOCK_SERVICE_URL = "http://vehicletracking.getsandbox.com/track/%s";

	private static final String PING_ID = "TK1ABC111";

	private Client client;

	public TrackingService() {
		ClientConfig clientConfig = new DefaultClientConfig();
		clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
		client = Client.create(clientConfig);
	}

	@Override
	public String getId() {
		return "trackingService";
	}

	@Override
	public ServiceState getServiceState() {
		// Service availability criteria: ping to the service by using a special tracking number
		try {
			getLocation(PING_ID);
		} catch (RuntimeException e) {
			new ServiceState(getId(), ServiceState.Status.NOT_AVAILABLE, "Tracking service is not working");
		}
		return new ServiceState(getId(), ServiceState.Status.FULLY_FUNCTIONAL, "Tracking service is working");
	}

	public Location getLocation(String trackingNo) {
		WebResource webResource = client.resource(String.format(MOCK_SERVICE_URL, trackingNo));
		webResource.accept("application/json; charset=utf-8").get(ClientResponse.class);
		ClientResponse response = webResource.get(ClientResponse.class);

		if (response.getStatus() != 200) {
			throw new WebApplicationException();
		}

		return response.getEntity(Location.class);
	}

}
