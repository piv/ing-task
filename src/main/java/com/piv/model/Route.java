package com.piv.model;

public class Route implements java.io.Serializable {

	private static final long serialVersionUID = -9005541596150573463L;

	public enum field {
		id, vehicleId, destination
	}

	private int id;
	private String vehicleId;
	private String destination;

	public Route() {
	}

	public Route(int id, String vehicleId, String destination) {
		this.id = id;
		this.vehicleId = vehicleId;
		this.destination = destination;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getVehicleId() {
		return vehicleId;
	}

	public void setVehicleId(String vehicleId) {
		this.vehicleId = vehicleId;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}
}
