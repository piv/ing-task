package com.piv.model;

public class Vehicle implements java.io.Serializable {

	private static final long serialVersionUID = 7024814261034796681L;

	public enum field {
		id, registration, capacity, trackingId
	}

	private int id;
	private String registration;
	private int capacity;
	private String trackingId;

	public Vehicle() {
	}

	public Vehicle(int id, String registration, int capacity, String trackingId) {
		this.id = id;
		this.registration = registration;
		this.capacity = capacity;
		this.trackingId = trackingId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRegistration() {
		return registration;
	}

	public void setRegistration(String registration) {
		this.registration = registration;
	}

	public int getCapacity() {
		return capacity;
	}

	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}

	public String getTrackingId() {
		return trackingId;
	}

	public void setTrackingId(String trackingId) {
		this.trackingId = trackingId;
	}

}
