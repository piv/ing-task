package com.piv.resource;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.piv.bean.ServiceState;
import com.piv.service.monitoring.MonitoredService;
import com.piv.service.monitoring.ServicesMonitoringService;

@Component
@Path("/monitoring")
public class MonitoringServiceResource {

	@Autowired
	@Resource
	ServicesMonitoringService servicesMonitoringService;

	@Path("/all")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public List<String> getAllMonitoredServices() {
		return Lists.transform(servicesMonitoringService.getAllServices(), new Function<MonitoredService, String>() {

			@Override
			public String apply(MonitoredService ms) {
				return ms.getId();
			}
		});
	}

	@Path("/app")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public ServiceState getSystem() {
		return servicesMonitoringService.getSystemStatus();
	}

	@Path("/service/{serviceId : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public ServiceState getServiceStatus(@PathParam("serviceId") final String serviceId) {
		MonitoredService service = servicesMonitoringService.getServiceById(serviceId);
		return service.getServiceState();
	}

	/*
	 * 
	 * @GET
	 * 
	 * @Path("/geojson")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") public String
	 * getGeoJSONForBoundingBox(@QueryParam("bbox") final String bbox) {
	 * 
	 * if (bbox == null) { return "{status:'failed', error:'bbox is null'}"; } try { final BoundingBox boundingBox = new
	 * BoundingBox(bbox); final List<JeanMonnetGeo> projects = jeanMonnetGeoDao.getAllProjectInBoundingBox(boundingBox);
	 * final String json = JeanMonnetGeoJSONBuilder.buildFeatureCollection(projects); return json; } catch (final
	 * Exception e) { return "{ status:'failed', error:'" + e.getMessage() + "'}"; }
	 * 
	 * // return "{ status:'ok' }"; }
	 * 
	 * @GET
	 * 
	 * @Path("/projects")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") public String getJeanMonneProjects(//
	 * 
	 * @QueryParam("bbox") final String bbox, //
	 * 
	 * @QueryParam("isocode") final String isoCode, //
	 * 
	 * @QueryParam("town") final String town, //
	 * 
	 * @QueryParam("year") final Integer year, //
	 * 
	 * @QueryParam("projectType") final String projectType) {
	 * 
	 * if (bbox == null) { return "{status:'failed', error:'bbox is null'}"; } try { final BoundingBox boundingBox = new
	 * BoundingBox(bbox); final List<JeanMonnetGeoBean> projects = jmGeoLocDao.getJeanMonneProjects(toBoundingBox(bbox),
	 * isoCode, town, year, projectType); final String json =
	 * JeanMonnetGeoBeanJSONBuilder.buildFeatureCollection(projects); return json; } catch (final Exception e) { return
	 * "{ status:'failed', error:'" + e.getMessage() + "'}"; } }
	 * 
	 * @GET
	 * 
	 * @Path("/countries")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") public List<Country> getCountries(//
	 * 
	 * @QueryParam("bbox") final String bbox, //
	 * 
	 * @QueryParam("town") final String town, //
	 * 
	 * @QueryParam("year") final Integer year, //
	 * 
	 * @QueryParam("projectType") final String projectType) {
	 * 
	 * return jmGeoLocDao.getCountries(toBoundingBox(bbox), town, year, projectType); }
	 * 
	 * @GET
	 * 
	 * @Path("/towns")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") public List<String> getTowns(//
	 * 
	 * @QueryParam("bbox") final String bbox, //
	 * 
	 * @QueryParam("isocode") final String isoCode, //
	 * 
	 * @QueryParam("year") final Integer year, //
	 * 
	 * @QueryParam("projectType") final String projectType) {
	 * 
	 * return jmGeoLocDao.getTowns(toBoundingBox(bbox), isoCode, year, projectType); }
	 * 
	 * @GET
	 * 
	 * @Path("/years")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") public List<Integer> getYears(//
	 * 
	 * @QueryParam("bbox") final String bbox, //
	 * 
	 * @QueryParam("isocode") final String isoCode, //
	 * 
	 * @QueryParam("town") final String town, //
	 * 
	 * @QueryParam("projectType") final String projectType) {
	 * 
	 * return jmGeoLocDao.getYears(toBoundingBox(bbox), isoCode, town, projectType); }
	 * 
	 * @GET
	 * 
	 * @Path("/projtypes")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8") public List<String> getProjectType(//
	 * 
	 * @QueryParam("bbox") final String bbox, //
	 * 
	 * @QueryParam("isocode") final String isoCode, //
	 * 
	 * @QueryParam("town") final String town, //
	 * 
	 * @QueryParam("year") final Integer year) {
	 * 
	 * return jmGeoLocDao.getProjectTypes(toBoundingBox(bbox), isoCode, town, year); }
	 */

	// ------------------------------------------------------------

	@GET
	@Path("/info")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInfo() {

		final String prefix = this.getClass().getAnnotation(Path.class).value();
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		boolean first = true;
		for (final Method method : this.getClass().getMethods()) {
			final Path methodAnn = method.getAnnotation(Path.class);
			if (methodAnn != null) {
				if (first) {
					first = false;
				} else {
					sb.append(",");
				}
				sb.append("\n\"").append(prefix).append(methodAnn.value());

				boolean firstQP = true;
				final Annotation[][] paramAnnotations = method.getParameterAnnotations();
				for (final Annotation[] pa : paramAnnotations) {
					for (final Annotation a : pa) {
						if (a.annotationType().equals(QueryParam.class)) {
							final QueryParam qp = (QueryParam) a;
							final String qpName = qp.value();
							if (firstQP) {
								sb.append("[?");
								firstQP = false;
							} else {
								sb.append("&");
							}
							sb.append(qpName).append("=<").append(qpName).append(">");
						}
					}
				}

				if (!firstQP) {
					sb.append(']');
				}

				sb.append('"');

			}

		}
		sb.append("\n]");
		return sb.toString();
	}

}
