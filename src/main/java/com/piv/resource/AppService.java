package com.piv.resource;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.List;

import javax.annotation.Resource;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.piv.bean.Address;
import com.piv.bean.Location;
import com.piv.bean.VehicleStatus;
import com.piv.dao.VehicleDao;
import com.piv.model.Vehicle;
import com.piv.service.AddressService;
import com.piv.service.TrackingService;

@Component
@Path("/get")
public class AppService {

	@Autowired
	@Resource
	VehicleDao vehicleDao;

	@Autowired
	@Resource
	TrackingService trackingService;

	@Autowired
	@Resource
	AddressService addressService;

	@Path("/vehicles")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public List<Vehicle> getAllVehicles() {
		return vehicleDao.getAll();
	}

	@Path("/vehicle/id/{vehicleId : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Vehicle getVehicleById(@PathParam("vehicleId") final int vehicleId) {
		return vehicleDao.findById(vehicleId);
	}

	@Path("/vehicle/reg/{regNo : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Vehicle getVehicleByRegNo(@PathParam("regNo") final String regNo) {
		return vehicleDao.findRegNo(regNo);
	}

	@Path("/vehicle/status/{regNo : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public VehicleStatus getVehicleStatusByRegNo(@PathParam("regNo") final String regNo) {
		Vehicle vehicle = vehicleDao.findRegNo(regNo);
		Location location = trackingService.getLocation(vehicle.getTrackingId());
		Address address = addressService.getAddress(location);
		VehicleStatus vehicleStatus = new VehicleStatus(address, location);
		return vehicleStatus;
	}

	// ----- used to test the services ---

	@Path("/vehicle/addr/{lat : .+}/{lon : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Address getAddressByLoc(@PathParam("lat") final String lat, @PathParam("lon") final String lon) {
		return addressService.getAddress(new Location(lat, lon));
	}

	@Path("/vehicle/location/{trackingId : .+}")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Location getVehicleLocation(@PathParam("trackingId") final String trackingId) {
		return trackingService.getLocation(trackingId);
	}

	// ------------------------------------------------------------

	@GET
	@Path("/info")
	@Produces(MediaType.APPLICATION_JSON)
	public String getInfo() {

		final String prefix = this.getClass().getAnnotation(Path.class).value();
		final StringBuilder sb = new StringBuilder();
		sb.append("[");
		boolean first = true;
		for (final Method method : this.getClass().getMethods()) {
			final Path methodAnn = method.getAnnotation(Path.class);
			if (methodAnn != null) {
				if (first) {
					first = false;
				} else {
					sb.append(",");
				}
				sb.append("\n\"").append(prefix).append(methodAnn.value());

				boolean firstQP = true;
				final Annotation[][] paramAnnotations = method.getParameterAnnotations();
				for (final Annotation[] pa : paramAnnotations) {
					for (final Annotation a : pa) {
						if (a.annotationType().equals(QueryParam.class)) {
							final QueryParam qp = (QueryParam) a;
							final String qpName = qp.value();
							if (firstQP) {
								sb.append("[?");
								firstQP = false;
							} else {
								sb.append("&");
							}
							sb.append(qpName).append("=<").append(qpName).append(">");
						}
					}
				}

				if (!firstQP) {
					sb.append(']');
				}

				sb.append('"');

			}

		}
		sb.append("\n]");
		return sb.toString();
	}
}
