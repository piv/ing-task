package com.piv.bean;

import java.io.Serializable;

public class ServiceState implements Serializable {

	private static final long serialVersionUID = 6018040532012041375L;

	public enum Status {
		NOT_AVAILABLE, FULLY_FUNCTIONAL
	}

	private final String serviceId;
	private final Status status;
	private final String info;

	public ServiceState(String serviceId, Status status, String info) {
		this.serviceId = serviceId;
		this.info = info;
		this.status = status;
	}

	public String getServiceId() {
		return serviceId;
	}

	public String getInfo() {
		return info;
	}

	public Status getStatus() {
		return status;
	}

	public String toString() {
		return String.format("ServiceState['%s','%s','%s']", serviceId, status, info);
	}

}
