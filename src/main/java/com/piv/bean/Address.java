package com.piv.bean;

import java.io.Serializable;

public class Address implements Serializable {

	private static final long serialVersionUID = 5188901254140478620L;

	private String country;
	private String city;
	private String street;

	public Address() {
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String toString() {
		return String.format("Address['%s','%s','%s']", street, city, country);
	}
}
