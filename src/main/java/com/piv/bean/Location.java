package com.piv.bean;

import java.io.Serializable;

public class Location implements Serializable {

	private static final long serialVersionUID = -3188031014014047212L;

	// These are doubles really
	private String lat;
	private String lon;

	public Location(String lat, String lon) {
		this.lat = lat;
		this.lon = lon;
	}

	public Location() {
	}

	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String toString() {
		return String.format("Location[%s,%s]", lat, lon);
	}
}
