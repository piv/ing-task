package com.piv.bean;

import java.io.Serializable;

public class VehicleStatus implements Serializable {

	private static final long serialVersionUID = -695649912043645915L;

	private Location location;
	private Address address;

	public VehicleStatus(Address address, Location location) {
		this.address = address;
		this.location = location;
	}

	public VehicleStatus() {
	}

	public Location getLocation() {
		return location;
	}

	public Address getAddress() {
		return address;
	}

}
