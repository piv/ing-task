Homework:
 
Small Java enterprise app
-              At least 1 database behind
-              2 other apps as dependency (you can mock them with anything)
-              TODO: implement monitoring services for the application and dependencies (DB and for the 2 apps), services should be REST.
 
Services expected:
-              One endpoint to check if the main application is running
-              One endpoint that should return the list of dependencies
-              One endpoint per dependency
 
Example for services:
-              Endpoint: http://server/monitoring/db, Response: db is Alive => similar for the other 2.
 
 
 



Design and Implementation:


The app here is a simple Vehicle Tracking System. It holds information about fleet of vehicles (in local HSQLDB database) 
each vehicle has a trackingId which is used to locate it using external TrackingService (Mock) - returns coordinates. 
Second external Address Service (Mocked) is used to get address from the coordinates. The app shows list of vehicle registration
 numbers and for each selected one retrieves the vehicle coordinates and address and display location on the map
 
 
 
 
 
 To build the app 
 
 mvn package
 
 It is run with the maven-jetty
 
 mvn jetty:run
 

 then can be loaded on http://localhost:8080/ 

 
 The rest services:
 
 Monitoring API
 
 http://localhost:8080/api/monitoring/app  - state of the Main app
 http://localhost:8080/api/monitoring/all  - All monitored services
 http://localhost:8080/api/monitoring/service/<service Id>   - the state of a service
 http://localhost:8080/api/monitoring/info - shows the rest interface
 
 UI/App sepcific API:
 http://localhost:8080/api/get/info - shows the rest interface
 http://localhost:8080/api/get/vehicles  - all vehicles in the database
 /get/vehicle/status/<registration No>  - return location and address for vehicle reg No
 
 
 
 
  
 Mock REST data
 
http://vehicletracking.getsandbox.com/track/TK1ABC111
{
  "lat": 50.850968,
  "lon": 4.352271
}

http://vehicletracking.getsandbox.com/track/TK2XYZ222
{
  "lat": 51.523344,
  "lon": -0.124037
}
 
http://vehicletracking.getsandbox.com/track/TK2TRA001
{
  "lat": 48.866672,
  "lon": 2.321888
} 

http://vehicletracking.getsandbox.com/track/TK1CAR002
{
  "lat": 41.905753,
  "lon": 12.496040
} 
 
 
 address service mock
 http://vehicletracking.getsandbox.com/address/50.850968/4.352271
 { "country":"Belgium", "city":"Brussels", "street":"De Brouckere" } 
 
 
 http://vehicletracking.getsandbox.com/address/51.523344/-0.124037
 { "country":"UK","city":"London", "street":"Bernard Street" }
 
 
 http://vehicletracking.getsandbox.com/address/48.866672/2.321888
 {
  "country": "France",
  "city": "Paris",
  "street": "Place de la Concorde"
}

http://vehicletracking.getsandbox.com/address/41.905753/12.496040
{
  "country": "Italy",
  "city": "Rome",
  "street": "Via Venti Settembre"
}
 

Vehicle table data:
 
 VEHICLE:'ID','REG_NO','CAPACITY','TRACKING_ID'
'1111,'1ABC111',100,'TK1ABC111'
2222,'2XYZ222',50,'TK2XYZ222'
3333,'2TRA001',150,'TK2TRA001'
4444,'1CAR002',50,'TK1CAR002'
 
 
 
 
 
 
 
 